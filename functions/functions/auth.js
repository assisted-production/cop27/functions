const functions = require('firebase-functions');
const admin = require('firebase-admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;
const db = admin.firestore();
const fs = require('fs');
const path = require('path');
const xlsx = require('xlsx');
const cors = require('cors')({
  origin: '*',
});

/**
 * Check if a given email address can submit (is in the whitelist) -- note that this now requires explicit 'pulic access' to be turned on in google cloudfunctions dashboard.
 */
exports.checkWhitelist = functions.https.onRequest((req, res) => {
  // console.log('trying whitelist');
  return cors(req, res, async () => {
    try {
      const email = req.body.email.toString();
      let doc = await db.collection('whitelist').doc(email).get();
      // if (doc. === null) throw new Error('no user');
      return res.send({ exists: doc.exists });
    } catch (error) {
      return res.status(401).send('Error checking whitelist');
    }
  });
});

/**
 * Get Firebase user profile using user id token
 */
exports.getProfile = functions.https.onRequest((req, res) => {
  return cors(req, res, async () => {
    const idToken = req.body.idToken.toString();

    try {
      const decodedClaims = await admin.auth().verifyIdToken(idToken);
      const userRecord = await admin.auth().getUser(decodedClaims.uid);
      return res.jsonp(userRecord.toJSON());
    } catch (error) {
      return res.status(401).send('Invalid Token');
    }
  });
});

/**
 * HTTP function for loading pre-defined user roles from the spreadsheet
 * for future references
 */
exports.loadUserRoles = functions.https.onRequest(async (req, res) => {
  try {
    // Load the spreadsheet using xlsx
    const buffer = fs.readFileSync(path.join(__dirname, '../scripts/userRoles.xlsx'));
    const workbook = xlsx.read(buffer, { type: 'buffer' });
    const sheetNameList = workbook.SheetNames;
    const xlData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);

    const promises = [];
    // Loop through the rows and parse the roles assigned to each user
    for (let i = 0; i < xlData.length; i++) {
      const { email, admin, moderator, transcriber, translator, verifier } = xlData[i];
      const roles = [];
      if (admin === 'T') roles.push('admin');
      if (moderator === 'T') roles.push('moderator');
      if (transcriber === 'T') roles.push('transcriber');
      if (translator === 'T') roles.push('translator');
      if (verifier === 'T') roles.push('verifier');

      promises.push(db.collection('config').doc('userRoles').collection('users').doc(email).set({ roles: roles }));
    }

    await Promise.all(promises);

    res.status(200).send('Successfully loaded user roles...');
  } catch (error) {
    functions.logger.error(error);
    res.status(400).send(error);
  }
});

/**
 * When new user account being created (via Google authentication), retrieve the user role
 * object from the config collection, check and assign the roles being allocated to this user
 */
exports.assignRoles = functions.auth.user().onCreate(async user => {
  functions.logger.info('user.uid: ', user.uid);
  functions.logger.info('user.email: ', user.email);

  // Fetch the pre-defined user roles from the config collection
  const userRolesSnapshot = await db.collection('config').doc('userRoles').collection('users').doc(user.email).get();
  const displayName = user.displayName ? user.displayName : 'Reporter';

  // Set the user roles if current user entry exists in the config file
  if (userRolesSnapshot.data()) {
    const roles = userRolesSnapshot.data().roles;
    await db.collection('users').doc(user.uid).set({
      roles: roles,
      createdAt: FieldValue.serverTimestamp(),
      updatedAt: FieldValue.serverTimestamp(),
      user_name: displayName,
      score: 0,
    });

    const userObj = { displayName: displayName };
    await admin.auth().updateUser(user.uid, userObj);
  }
  // Otherwise, create user in Firestore without roles (as participants)
  else
    await db.collection('users').doc(user.uid).set({
      createdAt: FieldValue.serverTimestamp(),
      updatedAt: FieldValue.serverTimestamp(),
      user_name: displayName,
      score: 0,
      isParticipant: true,
    });

  const userObj = { displayName: displayName };
  await admin.auth().updateUser(user.uid, userObj);
});

/**
 * Pubsub function for updating user names under the user object based on their
 * Firebase auth displayName field
 */
exports.updateUserName = functions.pubsub.schedule('every 10 minutes').onRun(async context => {
  try {
    const users = await db.collection('users').get();
    const uids = [];
    //get all users that are participants:
    users.forEach(user => {
      if (user.data().isParticipant) uids.push(user.id);
    });
    functions.logger.info('uids: ', uids);

    //load the user accounts for each one:
    const promises = [];
    for (let i = 0; i < uids.length; i++) {
      promises.push(admin.auth().getUser(uids[i]));
    }

    const results = await Promise.all(promises);
    const updatePromises = [];
    for (let i = 0; i < results.length; i++) {
      //if their name is not the default:
      if (results[i].displayName !== 'Reporter')
        //update their db name to match their account name
        updatePromises.push(db.collection('users').doc(uids[i]).update({ user_name: results[i].displayName }));
    }
    await Promise.all(updatePromises);
  } catch (error) {
    // functions.logger.error(error);
  }
});
