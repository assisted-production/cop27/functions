const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const _ = require('lodash');
const { ifrcList } = require('ifrc-list');

exports.adminListUsers = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '1GB',
  })
  .https.onCall(async (data, context) => {
    //TODO: DEBUG::
    let user = (await db.collection('users').doc(context.auth.uid).get()).data();
    if (!_.includes(user.roles, 'admin')) {
      throw new Error('Not authenticated');
    }

    //get all users ()
    let snapshot = await db.collection('users').where('isParticipant', '==', true).get();
    // let promises = [];

    // for (const doc of snapshot.docs) {
    //   promises.push(admin.auth().getUser(doc.id));
    // }

    let promises = snapshot.docs.map(async doc => {
      // console.log(doc.id);
      return {
        auth: await admin.auth().getUser(doc.id),
        user: doc.data(),
        submission: await db
          .collection('submissions')
          .where('submitted_by', '==', doc.ref)
          // .where('status', '==', 'edited')
          .get(),
        id: doc.id.substring(0, 5).toUpperCase(),
      };
    });

    let users = await Promise.all(promises);

    let mapped = _.map(users, u => {
      // console.log(u.submission);
      // if (u.submission.empty) return null;
      // else {
      // // console.log(ifrcList);
      // console.log(
      //   _.find(ifrcList, {
      //     code: u.submission.docs[0].data().region,
      //   }),
      // );
      let submitted = 0;
      let accepted = 0;

      if (!u.submission.empty) {
        submitted = _.size(u.submission.docs);
        accepted = _.size(
          _.filter(u.submission.docs, function (d) {
            return d.data().status == 'edited';
          }),
        );

        return `"${u.id}","${u.user.user_name}","reporter","${
          _.find(ifrcList, {
            code: u.submission.docs[0].data().region,
          }).ns
        }","${u.auth.email}","${submitted}","${accepted}"`;
      } else return null;
      // }
    });

    mapped = _.compact(mapped);

    return `"ID","Name","Role","NS","Email","Submitted","Accepted"\n${mapped.join('\n')}`;
  });

exports.adminListAdmins = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '1GB',
  })
  .https.onCall(async (data, context) => {
    let user = (await db.collection('users').doc(context.auth.uid).get()).data();
    if (!_.includes(user.roles, 'admin')) {
      throw new Error('Not authenticated');
    }

    //get all users from auth:
    let snapshot = await db.collection('users').orderBy('roles').orderBy('user_name').get();
    //get invites:
    let invites = await db.collection('config').doc('userRoles').collection('users').get();

    let promises = snapshot.docs.map(async doc => {
      return {
        auth: await admin.auth().getUser(doc.id),
        user: doc.data(),
      };
    });

    let users = await Promise.all(promises);

    //TODO: add on all the invites that dont have users:

    //for each invite, if its not in the user list, then add:

    console.log(_.map(invites.docs, m => m.data()));

    // console.log(users);

    return users;
  });

exports.adminListSubmissions = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '1GB',
  })
  .https.onCall(async (data, context) => {
    // exports.adminListSubmissions = functions.https.onRequest(async (req, res) => {

    //Check that this user is actually an admin

    //TODO: DEBUG::
    let user = (await db.collection('users').doc(context.auth.uid).get()).data();
    if (!_.includes(user.roles, 'admin')) {
      throw new Error('Not authenticated');
    }

    let pagesize = 0;
    let page = 0;
    let sort_by = 'submitted_by';
    let sort_dir = false;

    if (data.pagination) {
      pagesize = data.pagination.rowsPerPage || 5;
      page = data.pagination.page || 0;
      sort_by = data.pagination.sortBy || 'submitted_by';
      sort_dir = data.pagination.descending || false;
    }

    // Get all submissions matching the filter
    let snapshot = await db.collection('submissions').orderBy('phase').get();

    let submissions = [];
    for (const doc of snapshot.docs) {
      const dat = doc.data();
      dat.id = doc.id;
      submissions.push(dat);
    }

    // Group by submitted_by reference
    let grouped = _.groupBy(submissions, 'submitted_by.id');

    if (pagesize === 0) pagesize = _.size(grouped);

    let newobj = [];

    // console.log(Object.keys(grouped));

    for (const key in grouped) {
      // console.log('key: ', key);
      // let user = null;

      // console.log(user.uid);

      let subs = grouped[key];
      subs = _.orderBy(subs, 'createdAt', 'desc');

      let newsubs = [];
      for (let s of subs) {
        s = _.omit(s, 'submitted_by');
        newsubs.push(s);
      }
      let first = _.first(subs);

      newobj.push({
        submitted_by: key,
        lastTouched: first.createdAt,
        region: first.region,
        language: first.language,
        commslanguage: first.commslanguage,
        submissions: newsubs,
      });
    }

    // console.log(newobj);

    newobj = _.orderBy(newobj, [sort_by, 'region', 'language'], [sort_dir ? 'asc' : 'desc', 'asc', 'asc']);

    // Send back requested page (and total page info)
    let skip = (page - 1) * pagesize;
    // functions.logger.info("skip: ", skip);
    // functions.logger.info("pagesize: ", pagesize);

    let therest = _.drop(newobj, skip);
    let paged = _.take(therest, pagesize);

    let submitted_promises = [];
    let moderated_promises = [];

    for (let p of paged) {
      submitted_promises.push(admin.auth().getUser(p.submitted_by));

      for (let sub of p.submissions) {
        // console.log(`moderatedBy:`, sub);
        if (sub.moderatedBy) moderated_promises.push(admin.auth().getUser(sub.moderatedBy.replace('users/', '')));
        else moderated_promises.push(null);
      }

      for (let dat of p.submissions) {
        dat.media = _.first(dat.media).id;
      }
    }

    let results = await Promise.allSettled(submitted_promises);
    let subs_promises = await Promise.allSettled(moderated_promises);

    let i = 0;
    let j = 0;
    paged.forEach(obj => {
      obj.submitted_by =
        results[i].status === 'fulfilled'
          ? results[i].value
          : {
              uid: 'x' + i,
              displayName: 'Unknown',
            };
      i++;
      for (let sub of obj.submissions) {
        // console.log(subs_promises[j]);

        if (sub.moderatedBy) {
          sub.moderatedBy =
            subs_promises[j].status === 'fulfilled' && subs_promises[j].value
              ? { displayName: subs_promises[j].value.displayName }
              : { displayName: 'Unknown' };
        }
        j++;
      }
    });

    // return {};

    return {
      meta: {
        total: _.size(newobj),
        page,
        pagesize,
        descending: sort_dir,
        sortBy: sort_by,
      },
      data: paged,
    };
  });
