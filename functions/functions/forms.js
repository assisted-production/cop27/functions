const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const storage = admin.storage();
const FieldValue = admin.firestore.FieldValue;
const { getNoOfVideos } = require('../utils/utils');
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({ origin: true }));

// Get submissions for the current user
app.get('/:uid', async (req, res) => {
  const { uid } = req.params;

  try {
    const submissionSnapshots = await db
      .collection('submissions')
      .where('submitted_by', '==', db.collection('users').doc(uid))
      .orderBy('createdAt', 'asc')
      .get();
    const phaseVideos = await getNoOfVideos();

    const mediaPromises = [];
    const mediaStatusPromises = [];
    const mediaStatusIds = [];
    const submissions = [];
    const submissionIds = [];

    functions.logger.info('submissionSnapshots.size: ', submissionSnapshots.size);

    submissionSnapshots.docs.forEach(async doc => {
      const submission = doc.data();
      const mediaRefs = submission.media;
      submission.media = [];
      submission.id = doc.id;
      submissions.push(submission);
      submissionIds.push(doc.id);

      // If the submission is in status draft, fetch media upload status
      if (submission.status === 'draft' || submission.status === 'error_transcode') {
        functions.logger.info('submissionId: ', doc.id);
        const phase = submission.phase;
        const noOfVideos = phaseVideos[phase];

        for (let i = 0; i < noOfVideos; i++) {
          const rawMedia = mediaRefs[i];
          if (rawMedia) {
            mediaStatusIds.push(rawMedia.id);
            mediaStatusPromises.push(storage.bucket().file(`submissions/${doc.id}/${rawMedia.id}.mp4`).exists());
          }
        }
      }

      mediaRefs.forEach(media => {
        mediaPromises.push(media.get());
      });
    });

    const mediaResults = await Promise.all(mediaPromises);
    functions.logger.info('mediaResults: ', mediaResults);

    const mediaStatusResults = await Promise.all(mediaStatusPromises);

    for (let i = 0; i < mediaResults.length; i++) {
      if (mediaResults[i].data()) {
        const media = mediaResults[i].data();
        const submissionId = media.submission.split('/')[1];
        const index = submissionIds.indexOf(submissionId);
        if (media.langs) delete media.langs;
        media.id = mediaResults[i].id;

        // Attach media upload status if index exists in mediaStatusResults
        const mediaStatusIndex = mediaStatusIds.indexOf(media.id);
        if (mediaStatusIndex >= 0) media.uploadStatus = mediaStatusResults[mediaStatusIndex][0];
        submissions[index].media.push(media);
      }
    }

    res.status(200).send(submissions);
  } catch (error) {
    res.status(400).send(`Internal Server Error: ${error}`);
  }
});

// Post submission to Firestore
app.post('/', async (req, res) => {
  const { submission, phase, uid, noOfVideos } = req.body;
  functions.logger.info('req.body: ', req.body);

  try {
    const newSubmissionRef = db.collection('submissions').doc();
    const userRef = db.collection('users').doc(uid);

    const newMediaRefs = [];
    const newMediaIds = [];
    for (let i = 0; i < noOfVideos; i++) {
      const newMediaRef = db.collection('media').doc();
      newMediaRefs.push(newMediaRef);
      newMediaIds.push(newMediaRef.id);

      // Push the Firestore submission entry
      await newMediaRef.set({
        type: 'raw',
        createdAt: FieldValue.serverTimestamp(),
        submission: `submissions/${newSubmissionRef.id}`,
        src: `s3://cop27/submissions/${newSubmissionRef.id}/${newMediaRef.id}/${newMediaRef.id}_transcoded.mp4`,
      });
    }

    const newSubmission = submission;
    newSubmission.status = 'draft';
    newSubmission.submitted_by = userRef;
    newSubmission.phase = phase;
    newSubmission.media = newMediaRefs;
    newSubmission.createdAt = FieldValue.serverTimestamp();
    await newSubmissionRef.set(newSubmission);

    res.status(200).send({ newSubmissionId: newSubmissionRef.id, newMediaIds: newMediaIds });
  } catch (error) {
    res.status(500).send(`Post Submission Error: ${error}`);
  }
});

// Get submission status
app.get('/submission/:id', async (req, res) => {
  const { id } = req.params;
  try {
    const snapshot = await db.collection('submissions').doc(id).get();
    res.status(200).send({ status: snapshot.data().status });
  } catch (error) {
    res.status(500).send(`Get Submission Status Error: ${error}`);
  }
});

// Update submission status to submitted
app.post('/submission/:id', async (req, res) => {
  // Check authorization header of the request
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
    return res.status(403).send('Unauthorized');

  const idToken = req.headers.authorization.split('Bearer ')[1];
  const { id } = req.params;
  const { status, uid } = req.body;

  try {
    // Decode the id token for authentication
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    functions.logger.info('Firestore id token correctly decoded: ', decodedIdToken);

    const submissionRef = db.collection('submissions').doc(id);
    const submissionUid = (await submissionRef.get()).data().submitted_by.id;
    if (submissionUid !== decodedIdToken.uid) return res.status(403).send('Unauthorized');

    // If validation passed, update the submission status
    await submissionRef.update({ status: status });
    res.status(200).send(`Successfully updated the submission status...`);
  } catch (error) {
    res.status(500).send(`Update Submission Status Error: ${error}`);
  }
});

exports.submissions = functions
  .runWith({
    memory: '1GB',
  })
  .https.onRequest(app);
