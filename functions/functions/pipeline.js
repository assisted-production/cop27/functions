const functions = require('firebase-functions');
const admin = require('firebase-admin');
const environment = functions.config();
const db = admin.firestore();
const storage = admin.storage();
const FieldValue = admin.firestore.FieldValue;
const { getNoOfVideos, uploadCaptionToS3 } = require('../utils/utils');
const path = require('path');
const os = require('os');
const fs = require('fs');
const parseSRT = require('srt-to-json');
const AWS = require('aws-sdk');
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
AWS.config.mediaconvert = { endpoint: environment.aws.mediaconvert_endpoint };
const S3 = new AWS.S3();
// const request = require('request');
const BUCKET_NAME = 'cop27';
const { getMediaConvertParams } = require('../utils/mediaConvertParams.js');
// const { sendResubmitEmail } = require('../utils/emailing');
const { speechCall, downloadS3Resource } = require('../utils/googleSpeech');
const { insertYouTubeCaption } = require('../utils/youtube');
const request = require('request');

/**
 * Detect the Firebase Storage onFinalize event and push the submission video to Amazon S3
 * for starting the transcoding & transcription job
 */
exports.pushToAmazonS3 = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '4GB',
  })
  .storage.object()
  .onFinalize(async object => {
    const fileBucket = object.bucket;
    const tempFilePath = object.name;
    const fileName = path.basename(tempFilePath);
    const contentType = object.contentType;

    // functions.logger.info('KEY: ', AWS.config.credentials.accessKeyId);

    functions.logger.info('fileBucket: ', fileBucket);
    functions.logger.info('fileName: ', fileName);
    functions.logger.info('contentType: ', contentType);
    const filePath = `${tempFilePath.split('.')[0]}/${fileName}`;
    functions.logger.info('tempFilePath: ', tempFilePath);

    // If the storage item is a .mp3 file, push the file to Google Speech-to-Text transcription
    // and generate the VTT file accordingly
    if (tempFilePath.endsWith('.mp3')) {
      const moderation = fileName.startsWith('moderation_') ? true : false;
      let language;
      if (moderation) language = fileName.split('_')[1];
      else language = fileName.split('_')[0];
      functions.logger.info('language: ', language);

      const vtt = await speechCall(tempFilePath, moderation, language);
      functions.logger.info('vtt: ', vtt);

      // If moderation, then update the raw_transcript with the transcription
      // from Google Speech-to-Text
      if (moderation) {
        const submissionId = tempFilePath.split('/')[1];
        await updateRawTranscript(submissionId, language, vtt);
      }

      // Otherwise, generate the vtt file and push it to YouTube
      // Create a media object with type `caption `for referencing the vtt
      else {
        const submissionId = tempFilePath.split('/')[1];
        const mediaObj = await getHdMediaId(submissionId);
        const mediaId = mediaObj.id;
        const youtube = mediaObj.data().youtube;
        functions.logger.info('mediaId: ', mediaId);
        functions.logger.info('youtube: ', youtube);

        // YouTube video push is triggered by `onSubmissionStatusTrigger` of changing the
        // submission status to `edited`, if the youtube id does not exist yet, need to
        // convert the job into a queue item and process later
        if (youtube) {
          if (mediaId) {
            await insertYouTubeCaption(mediaId, language, vtt);
            await uploadCaptionToS3(submissionId, mediaId, language, vtt, youtube);
          }
        } else {
          // Push the youtube caption insert job into a queue and process later
          const youtubeJobRef = db.collection('captionInsertJobs').doc();
          await youtubeJobRef.set({
            createdAt: FieldValue.serverTimestamp(),
            mediaId,
            submissionId,
            language,
            vtt,
          });
        }
      }
    }

    // Return if the file is not uploaded to the submissions folder
    if (!tempFilePath.startsWith('submissions') || tempFilePath.endsWith('.mp3')) return;

    try {
      // Check whether all videos for the submission have been uploaded, and change the
      // submission status to be `submitted` if so
      await checkAndUpdateSubmissionStatus(tempFilePath.split('/')[1]);

      // Start transcoding & transcription pipeline
      const localFileName = await downloadVideo(fileBucket, tempFilePath, fileName);
      functions.logger.info('localFileName: ', localFileName);

      // Read content from the file
      const fileContent = fs.readFileSync(localFileName);

      // Setting up S3 upload parameters
      const params = {
        Bucket: BUCKET_NAME,
        Key: filePath,
        Body: fileContent,
      };

      await S3.upload(params).promise();
      s3Url = `s3://${BUCKET_NAME}/${filePath}`;
      fs.unlinkSync(localFileName);

      // Trigger the transcoding and transcription job once the video being uploaded to S3
      await createMediaConvertJob(s3Url, filePath);
      await createTranscriptionJob(s3Url, fileName, filePath);
    } catch (error) {
      throw error;
    }
  });

const handleSubscriptionResponse = function (error, response) {
  if (!error && response.statusCode === 200) {
    console.log('Yes! We have accepted the confirmation from AWS.');
  } else {
    throw new Error(`Unable to subscribe to given URL`);
  }
};

const parseEditCompleteEvent = async function (outputGroup) {
  //write hd_video and hd_video_transcoded media objects:
  //set submission to status='edited'
  const submissionId = outputGroup[0].outputFilePaths[0].split('/')[4];

  const s3ObjectKey = outputGroup[0].outputFilePaths[0];

  //create media objects for hd_video and hd_video_transcoded
  const docRefHD = await db.collection('media').add({
    src: s3ObjectKey,
    type: `hd_video`,
    // srcLang: srcLang,
    status: 'edited',
    createdAt: FieldValue.serverTimestamp(),
    submission: `submissions/${submissionId}`,
  });

  const docRefHDTrans = await db.collection('media').add({
    src: s3ObjectKey.replace('.mp4', 'hd_video_transcoded.mp4'),
    type: `hd_video_transcoded`,
    // srcLang: srcLang,
    status: 'transcoded',
    createdAt: FieldValue.serverTimestamp(),
    submission: `submissions/${submissionId}`,
  });

  await db
    .collection('submissions')
    .doc(submissionId)
    .update({
      media: admin.firestore.FieldValue.arrayUnion(docRefHD, docRefHDTrans),
      status: 'edited',
    });
};

/**
 * Endpoint for AWS SNS notification triggers when video transcoding / transcription job finishes
 * 1. MediaConvertJobCompleteAlert: Triggered when video has been transcoded (either the initial submissions or an edit)
 * 2. TranscribeJobCompleteAlert: Triggered when video has been transcribed (when new transcript
 * being pushed to Amazon S3 bucket)
 */
exports.awsSns = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '256MB',
  })
  .https.onRequest(async (req, res) => {
    try {
      // Parse the request payload and extract the notification topic
      const payload = JSON.parse(req.body);
      if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
        console.log(payload);
        const url = payload.SubscribeURL;
        await request(url, handleSubscriptionResponse);
        return res.send('OK');
      } else {
        // const topic = payload.TopicArn.split(':')[5];
        // functions.logger.info('topic: ', topic);

        // Parse the request payload and extract the notification topic
        // const payload = JSON.parse(req.body);
        const topic = payload.TopicArn.split(':')[5];
        functions.logger.info('topic: ', topic);
        let message = JSON.parse(payload.Message);

        functions.logger.info(message);

        // If the topic is `MediaConvertJobCompleteAlert`, loop through all the outputs and
        // 1. Create media objects for each thumbnail generated
        // 2. Update the status flag to `transcoded` or `readyformoderation`
        if (topic === 'MediaConvertJobCompleteAlert' && message.detail.status === 'COMPLETE') {
          const outputGroup = message.detail.outputGroupDetails[0].outputDetails;
          functions.logger.info('outputGroup: ', JSON.stringify(outputGroup));

          //outputGroup[0].outputFilePaths[0].split('/')[4]
          if (outputGroup[0].outputFilePaths[0].endsWith('hd_video.mp4')) {
            await parseEditCompleteEvent(outputGroup);
          } else {
            await parseTranscodeJobOutput(outputGroup);
          }
          res.writeHead(200);
        } else if (topic === 'TranscribeJobCompleteAlert') {
          const s3ObjectKey = message.Records[0].s3.object.key;
          functions.logger.info('s3ObjectKey: ', s3ObjectKey);
          const submissionId = s3ObjectKey.split('/')[1];

          // Return if the submission does not exists
          const subSnap = await db.collection('submissions').doc(submissionId).get();
          if (!subSnap.exists) return res.end();

          const TranscriptFileJSON = await getAWSTranscript(s3ObjectKey);
          await updateTransribeStatus(submissionId, TranscriptFileJSON);
          res.writeHead(200);
        } else if (topic === 'MediaConvertJobErrorAlert') {
          // message = JSON.parse(payload.jsonPayload.Message);

          // console.log(message);

          if (message.detail.status === 'ERROR') {
            functions.logger.info('Processing transcode error:', message);

            const { jobId } = message.detail;
            const errorMessage = 'transcoding_failed';
            const mediaConvert = new AWS.MediaConvert();
            const data = await mediaConvert.getJob({ Id: jobId }).promise();

            //if its a single file (i.e. its failed when the initial videos are uploaded)
            if (data.Job.Settings.Inputs.length === 1) {
              const fileInput = data.Job.Settings.Inputs[0].FileInput;
              const sid = fileInput.split('/')[4];

              // functions.logger.info('job', data);

              // Return if the submission does not exists
              const subSnap = await db.collection('submissions').doc(sid).get();
              if (!subSnap.exists) return res.end();

              const mid = fileInput.split('/')[5];
              const fileName = fileInput.split('/')[6];
              functions.logger.info(`MediaConvertJobErrorAlert for ${sid}`);

              // Update error messages in Firestore if it is hd_video transcoding
              if (!fileName.startsWith('hd_video')) {
                await db.collection('media').doc(mid).update({ status: 'error', error: errorMessage });

                const submissionRef = db.collection('submissions').doc(sid);

                // Send resubmit notification email
                // const submissionSnap = await submissionRef.get();
                // const uid = submissionSnap.data().submitted_by.id;
                // const commsLang = submissionSnap.data().commslanguage;

                //only send/update if the submission is not already in an error state:
                // if (submissionSnap.data().status !== 'error') {

                await submissionRef.update({
                  status: 'error',
                  error: errorMessage,
                });
                // }
              }
              // Otherwise, get the transcoded id from the current media object
              else {
                const mediaSnapshot = await db.collection('media').doc(mid).get();

                // Update the error status in the transcoded hd_video media object
                const { transcodedId } = mediaSnapshot.data();
                await db.collection('media').doc(transcodedId).update({ status: 'error', error: errorMessage });
              }
            } else {
              //its an edit (i.e. more than a single input file):
              const fileInput = data.Job.Settings.OutputGroups[0].OutputGroupSettings.FileGroupSettings.Destination;
              const sid = fileInput.split('/')[4];

              // functions.logger.info('job', data);

              // Return if the submission does not exists
              const subSnap = await db.collection('submissions').doc(sid).get();
              if (!subSnap.exists) return res.end();

              // const mid = fileInput.split('/')[5];
              // const fileName = fileInput.split('/')[6];
              functions.logger.info(`MediaConvertJobErrorAlert for ${sid}`);

              // Update error messages in Firestore if it is hd_video transcoding
              // if (!fileName.startsWith('hd_video')) {
              // await db.collection('media').doc(mid).update({ status: 'error', error: errorMessage });

              const submissionRef = db.collection('submissions').doc(sid);
              await submissionRef.update({
                status: 'error',
                error: 'editing_failed',
              });

              // Send resubmit notification email
              // const submissionSnap = await submissionRef.get();
              // const uid = submissionSnap.data().submitted_by.id;
              // const commsLang = submissionSnap.data().commslanguage;
              // await sendResubmitEmail(uid, commsLang);
            }
          }
        } else if (topic === 'TranscribeJobErrorAlert') {
          const jobName = message.detail.JobName;
          const mid = jobName.split('.')[0];
          const mSnapshot = await db.doc(`media/${mid}`).get();
          const sid = mSnapshot.data().submission.split('/')[1];

          // Return if the submission does not exists
          const subSnap = await db.collection('submissions').doc(sid).get();
          if (!subSnap.exists) return res.end();

          functions.logger.info(`TranscribeJobErrorAlert for ${sid}`);

          // Create new media object for `raw_transcript` with error status
          const newMediaRef = db.collection('media').doc();
          const transcriptObject = {
            type: 'raw_transcript',
            createdAt: FieldValue.serverTimestamp(),
            submission: `submissions/${sid}`,
            status: 'error',
            error: 'transcription_failed',
          };
          await newMediaRef.set(transcriptObject);
          await db.doc(`/submissions/${sid}`).update({
            media: admin.firestore.FieldValue.arrayUnion(newMediaRef),
          });

          if (await finishAllTranscription(sid)) await updateSubmissionStatus(sid, false);
        } else res.writeHead(400);
        res.end();
      }
    } catch (error) {
      console.error(error);
      res.writeHead(400);
      res.end();
    }
  });

/**
 * Parse the MediaConvert transcoding job output and create media objects for each thumbnail
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
async function parseTranscodeJobOutput(outputGroup) {
  const submissionId = getSubmissionId(outputGroup);

  // Return if the submission does not exists
  const subSnap = await db.collection('submissions').doc(submissionId).get();
  if (!subSnap.exists) return;

  const mediaId = getMediaId(outputGroup);
  const fileName = getFileName(outputGroup);
  const thumbnailKey = getThumbnailKey(outputGroup);
  const audioKey = getAudioKey(outputGroup);
  const durationInMs = outputGroup[0].durationInMs;
  const language = subSnap.data().language;
  functions.logger.info('thumbnailKey: ', thumbnailKey);
  functions.logger.info('audioKey: ', audioKey);
  functions.logger.info('durationInMs: ', durationInMs);
  functions.logger.info('language: ', language);
  if (thumbnailKey.length > 0) await createThumbnailObjects(submissionId, thumbnailKey);

  // If the job is sqaure_video editing, update the edit duration
  if (fileName.startsWith('square_video')) {
    const mediaSnapshot = await db.collection('media').doc(mediaId).get();

    // Update the status to be `transcoded`
    const { transcodedId } = mediaSnapshot.data();
    await db.collection('media').doc(transcodedId).update({ status: 'transcoded', duration: durationInMs });
  } else if (fileName.startsWith('feedback')) {
    // Update the status flag of the judgingEditJob with the S3 url
    const cloudfrontUrl = (await db.collection('config').doc('meta').get()).data().cloudfronturl;
    const s3Url = `${cloudfrontUrl}/submissions/${submissionId}/${mediaId}/${fileName}`;
    functions.logger.info('s3Url: ', s3Url);

    // // Generate the transcript
    // const feedbackTranscriptMediaObj = await getFeedbackTranscriptMediaId(submissionId);
    // const transcriptS3Key = `submissions/${submissionId}/${feedbackTranscriptMediaObj.id}/feedback_transcript.srt`;
    // const transcript = await getConcatTranscript(transcriptS3Key);

    await db.collection('judgingeditjobs').doc(submissionId).update({
      status: 'finalised',
      src: s3Url,
    });
  } else {
    // If not transcoding for `hd_video`, then update transcoded status
    if (!fileName.startsWith('hd_video')) {
      // Bengali bn, Persian fa, Punjabi pa, Urdu ur, Swahili sw
      // If in those langauges, transcription will fail. Use Google Speech-to-Text
      // to create raw transcript objects
      if (['bn', 'fa', 'pa', 'ur', 'sw'].indexOf(language) >= 0)
        await downloadS3Resource(submissionId, audioKey, true, language);

      await db.collection('media').doc(mediaId).update({ status: 'transcoded', duration: durationInMs });

      // Update submission status if all video has been transcoded
      const allTranscoded = await finishAllTranscoding(submissionId);
      if (allTranscoded) await updateSubmissionStatus(submissionId, true);
    }
    // Otherwise, fetch the transcoded media id from the current media object
    else {
      // Bengali bn, Persian fa, Punjabi pa, Urdu ur, Swahili sw, Arabic ar, Hindi hi, Chinese, zh
      if (['bn', 'fa', 'pa', 'ur', 'sw', 'ar', 'hi', 'zh'].indexOf(language) >= 0)
        await downloadS3Resource(submissionId, audioKey, false, language);

      const mediaSnapshot = await db.collection('media').doc(mediaId).get();

      // Update the status to be `transcoded`
      const { transcodedId } = mediaSnapshot.data();
      await db.collection('media').doc(transcodedId).update({ status: 'transcoded', duration: durationInMs });
    }
  }
}

/**
 * Get submission id from the MediaConvert output group
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
function getSubmissionId(outputGroup) {
  return outputGroup[0].outputFilePaths[0].split('/')[4];
}

/**
 * Get media id from the MediaConvert output group
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
function getMediaId(outputGroup) {
  return outputGroup[0].outputFilePaths[0].split('/')[5];
}

/**
 * Get file name from the MediaConvert output group
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
function getFileName(outputGroup) {
  return outputGroup[0].outputFilePaths[0].split('/')[6];
}

/**
 * Get the thumbnail S3 keys for creating media objects
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
function getThumbnailKey(outputGroup) {
  const thumbnailKey = [];
  for (let i = 0; i < outputGroup.length; i++) {
    const s3FileKey = outputGroup[i].outputFilePaths[0];
    const temp = s3FileKey.split('.');
    const fileType = temp[temp.length - 1];
    if (fileType === 'jpg') thumbnailKey.push(s3FileKey);
  }
  return thumbnailKey;
}

/**
 * Get the audio S3 keys for creating media objects
 *
 * @param {*} outputGroup Output group for AWS MediaConvert Job
 */
function getAudioKey(outputGroup) {
  let audioKey;
  for (let i = 0; i < outputGroup.length; i++) {
    const s3FileKey = outputGroup[i].outputFilePaths[0];
    const temp = s3FileKey.split('.');
    const fileType = temp[temp.length - 1];
    if (fileType === 'mp3') {
      audioKey = s3FileKey;
      break;
    }
  }
  return audioKey;
}

/**
 * Create thumbnail media objects in Firestore with the given keys
 *
 * @param {*} submissionId User submission id
 * @param {*} thumbnailKey Array of thumbnail keys in S3
 */
async function createThumbnailObjects(submissionId, thumbnailKey) {
  const promises = [];
  const mediaRefs = [];
  const key = thumbnailKey[0];
  const keyArr = getKeyArr(key);

  for (let i = 0; i < keyArr.length; i++) {
    const newMediaRef = db.collection('media').doc();
    mediaRefs.push(newMediaRef);

    promises.push(
      newMediaRef.set({
        type: 'image',
        src: keyArr[i],
        createdAt: FieldValue.serverTimestamp(),
        submission: `submissions/${submissionId}`,
      }),
    );
  }

  try {
    await Promise.all(promises);

    const snapshot = await db.doc(`/submissions/${submissionId}`).get();
    // Create or add media file for the submissions
    if (!snapshot.data().media) await db.doc(`/submissions/${submissionId}`).update({ media: mediaRefs });
    else
      await db.doc(`/submissions/${submissionId}`).update({
        media: admin.firestore.FieldValue.arrayUnion(...mediaRefs),
      });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the transcript created by AWS Transribe with the given S3 object key
 *
 * @param {*} s3ObjectKey S3 object key
 */
async function getAWSTranscript(s3ObjectKey) {
  const params = {
    Bucket: BUCKET_NAME,
    Key: s3ObjectKey,
  };

  // Download the transcript file and parse the object
  const TranscriptFile = await S3.getObject(params).promise();
  return JSON.parse(TranscriptFile.Body.toString('utf-8'));
}

/**
 * Update the transcription status and create media objects for the transcript
 *
 * @param {*} submissionId User submission id
 * @param {*} TranscriptFileJSON Transcript file in JSON format
 */
async function updateTransribeStatus(submissionId, TranscriptFileJSON) {
  const identification = TranscriptFileJSON.results.language_identification;
  const transcript = TranscriptFileJSON.results.transcripts[0].transcript;
  const lang = TranscriptFileJSON.results.language_code.split('-')[0];

  functions.logger.info('submissionId: ', submissionId);
  functions.logger.info('identification: ', identification);
  functions.logger.info('transcript: ', transcript);
  functions.logger.info('lang: ', lang);

  const snapshot = await db.doc(`/submissions/${submissionId}`).get();

  // Create media object for translation
  const newMediaRef = db.collection('media').doc();
  const newLangsRef = db.collection('langs').doc();
  const transcriptObject = {
    type: 'raw_transcript',
    identification: identification,
    modality: 'text',
    createdAt: FieldValue.serverTimestamp(),
    submission: `submissions/${submissionId}`,
    langs: [newLangsRef],
  };

  await newLangsRef.set({
    media: `media/${newMediaRef.id}`,
    createdAt: FieldValue.serverTimestamp(),
    original: true,
    srcLang: lang,
    srcText: transcript,
  });
  await newMediaRef.set(transcriptObject);

  // Create or add media file for the submissions
  if (!snapshot.data().media) await db.doc(`/submissions/${submissionId}`).update({ media: [newMediaRef] });
  else
    await db.doc(`/submissions/${submissionId}`).update({
      media: admin.firestore.FieldValue.arrayUnion(newMediaRef),
    });

  // Update submission status if all video has been transcribed
  if (await finishAllTranscription(submissionId)) await updateSubmissionStatus(submissionId, false);
}

/**
 * Download the video from Firebase storage to the given file path
 *
 * @param {*} fileBucket Firebase Storage buckect name
 * @param {*} filePath File path in Firebase Storage
 * @param {*} fileName Name of the file for constructing the tempFilePath
 */
async function downloadVideo(fileBucket, filePath, fileName) {
  const bucket = admin.storage().bucket(fileBucket);
  const tempFilePath = path.join(os.tmpdir(), fileName);
  await bucket.file(filePath).download({ destination: tempFilePath });
  return tempFilePath;
}

/**
 * Create a media convert job on AWS with defined parameters
 *
 * @param {String} url S3 URL of the video
 * @param {String} filePath Video file path in S3
 */
function createMediaConvertJob(url, filePath) {
  const filePathArr = filePath.split('/');
  const destindation = `s3://cop27/submissions/${filePathArr[1]}/${filePathArr[2]}/`;
  const params = getMediaConvertParams(url, '_transcoded', destindation);

  // Create a promise on a MediaConvert object
  return new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();
}

/**
 * Create the AWS transcribe job with the submission video
 *
 * @param {*} url S3 URL of the video
 * @param {*} fileName Transcription job name
 * @param {*} filePath Video file path in S3
 */
async function createTranscriptionJob(url, fileName, filePath) {
  const transcribeService = new AWS.TranscribeService();
  const TranscriptionJobName = fileName;
  const filePathArr = filePath.split('/');
  const transcriptionJob = {
    IdentifyLanguage: true,
    Media: { MediaFileUri: url },
    MediaFormat: 'mp4',
    TranscriptionJobName,
    OutputBucketName: BUCKET_NAME,
    OutputKey: `submissions/${filePathArr[1]}/${filePathArr[2]}/`,
  };

  try {
    await transcribeService
      .getTranscriptionJob({
        TranscriptionJobName,
      })
      .promise();

    // Remove the existing transcription job if it already exists
    await transcribeService.deleteTranscriptionJob({ TranscriptionJobName }).promise();
    return transcribeService.startTranscriptionJob(transcriptionJob).promise();
  } catch (error) {
    // If error occurred, the job does not exist and no action needed
    return transcribeService.startTranscriptionJob(transcriptionJob).promise();
  }
}

/**
 * Pad the number with a given size
 *
 * @param {*} num Number for padding
 * @param {*} size Size of the integer string
 */
function pad(num, size) {
  var str = '0000000' + num;
  return str.substr(str.length - size);
}

/**
 * Generate thumbnail keys for the transcoding job since SNS will only notify the last thumbnail
 * being generated, and we need to construct the keys for storgae
 *
 * @param {*} key S3 file key
 */
function getKeyArr(key) {
  const temp = key.split('.');
  const index = temp[temp.length - 2];
  const keyArr = [key];

  const origin = pad(index, 7);
  for (let j = index - 1; j >= 0; j--) {
    const idx = pad(j, 7);
    const newKey = key.replace(origin, idx);
    keyArr.push(newKey);
  }

  return keyArr;
}

/**
 * Update the submission status to `transcoded`, `transcribed`, or `readyformoderation`
 *
 * @param {*} sid Submission Id of the video
 * @param {*} transcode Flag indicating whether it is an update for transcoding complete
 */
async function updateSubmissionStatus(sid, transcode) {
  const snapshot = await db.doc(`/submissions/${sid}`).get();
  if (snapshot.data().status === 'error') return;

  if (transcode) {
    // Update the status to be `readyformoderation` if already transcribed
    const allTranscribed = await finishAllTranscription(sid);
    if (snapshot.data().status === 'transcribed' || allTranscribed)
      await snapshot.ref.update({ status: 'readyformoderation' });
    // Otherwise, set the status to be `transcoded`
    else await snapshot.ref.update({ status: 'transcoded' });
  } else {
    // Update the status to be `readyformoderation` if already transcoded
    const allTranscoded = await finishAllTranscoding(sid);
    if (snapshot.data().status === 'transcoded' || allTranscoded)
      await snapshot.ref.update({ status: 'readyformoderation' });
    // Otherwise, set the status to be `transcribed`
    else await snapshot.ref.update({ status: 'transcribed' });
  }
}

async function finishAllTranscription(sid) {
  const phaseVideos = await getNoOfVideos();
  const submissionRef = db.collection('submissions').doc(sid);
  const snapshot = await submissionRef.get();
  const noOfVideos = phaseVideos[snapshot.data().phase];
  const transcriptSnapshots = await db
    .collection('media')
    .where('submission', '==', `submissions/${sid}`)
    .where('type', '==', 'raw_transcript')
    .get();

  functions.logger.info('finishAllTranscription: ', transcriptSnapshots.size >= noOfVideos);
  return transcriptSnapshots.size >= noOfVideos;
}

async function finishAllTranscoding(sid) {
  const phaseVideos = await getNoOfVideos();
  const submissionRef = db.collection('submissions').doc(sid);
  const snapshot = await submissionRef.get();
  const noOfVideos = phaseVideos[snapshot.data().phase];
  const transcodeSnapshots = await db
    .collection('media')
    .where('submission', '==', `submissions/${sid}`)
    .where('status', '==', 'transcoded')
    .get();
  functions.logger.info('finishAllTranscoding: ', transcodeSnapshots.docs.length === noOfVideos);
  return transcodeSnapshots.docs.length === noOfVideos;
}

/**
 * If submission status is draft, check whether all the videos have been uploaded, and
 * set the status to be submitted if so
 *
 * @param {*} submissionId Submission Id
 */
async function checkAndUpdateSubmissionStatus(submissionId) {
  functions.logger.info('submissionId: ', submissionId);
  const submissionRef = db.collection('submissions').doc(submissionId);
  const snapshot = await submissionRef.get();
  const submission = snapshot.data();

  const mediaStatusPromises = [];
  if (submission.status === 'draft') {
    const phaseVideos = await getNoOfVideos();
    const phase = submission.phase;
    const noOfVideos = phaseVideos[phase];

    for (let i = 0; i < noOfVideos; i++) {
      const rawMedia = submission.media[i];
      mediaStatusPromises.push(storage.bucket().file(`submissions/${submissionId}/${rawMedia.id}.mp4`).exists());
    }
  }
  const mediaStatusResults = await Promise.all(mediaStatusPromises);
  functions.logger.info('mediaStatusResults: ', mediaStatusResults);

  const allUploaded = mediaStatusResults.every(m => m[0] === true);
  functions.logger.info('allUploaded: ', allUploaded);

  if (submission.status !== 'error_transcode' && allUploaded) await submissionRef.update({ status: 'submitted' });
}

/**
 * Update the raw transcript if transcription is not supported by AWS
 *
 * @param {*} submissionId Submission ID
 * @param {*} lang Language of the submission
 * @param {*} transcript Transcript for the submission
 */
async function updateRawTranscript(submissionId, lang, transcript) {
  try {
    const newMediaRef = db.collection('media').doc();
    const newLangsRef = db.collection('langs').doc();

    const transcriptObject = {
      type: 'raw_transcript',
      modality: 'text',
      createdAt: FieldValue.serverTimestamp(),
      submission: `submissions/${submissionId}`,
      langs: [newLangsRef],
    };

    await newLangsRef.set({
      media: `media/${newMediaRef.id}`,
      createdAt: FieldValue.serverTimestamp(),
      original: true,
      srcLang: lang,
      srcText: transcript,
    });
    await newMediaRef.set(transcriptObject);

    await db.doc(`/submissions/${submissionId}`).update({
      media: admin.firestore.FieldValue.arrayUnion(newMediaRef),
    });
  } catch (error) {
    functions.logger.error(error);
  }
}

/**
 * Get hd_video media object
 *
 * @param {*} sid Submission ID
 */
async function getHdMediaId(sid) {
  const mediaObjects = await db.collection('media').where('submission', '==', `submissions/${sid}`).get();

  let mediaObj;
  for (let i = 0; i < mediaObjects.size; i++) {
    const media = mediaObjects.docs[i];
    if (media.data().type === 'hd_video' && media.data().status !== 'exporting') {
      mediaObj = media;
      break;
    }
  }
  return mediaObj;
}

/**
 * Get hd_submission_feedback_transcriptvideo media object
 *
 * @param {*} sid Submission ID
 */
async function getFeedbackTranscriptMediaId(sid) {
  const mediaObjects = await db.collection('media').where('submission', '==', `submissions/${sid}`).get();

  let mediaObj;
  for (let i = 0; i < mediaObjects.size; i++) {
    const media = mediaObjects.docs[i];
    if (media.data().type === 'submission_feedback_transcript' && media.data().status === 'ready') {
      mediaObj = media;
      break;
    }
  }
  return mediaObj;
}

/**
 * Generate concatenated transcript from the .srt file in S3
 *
 * @param {*} transcriptS3Key S3 Key for the feedback transcript
 */
async function getConcatTranscript(transcriptS3Key) {
  // Create read stream from S3 bucket
  const s3data = {
    Bucket: BUCKET_NAME,
    Key: transcriptS3Key,
  };
  const s3Response = await S3.getObject(s3data).promise();
  const srtRaw = s3Response.Body.toString();

  const srtJson = parseSRT(srtRaw);
  let feedback = '';
  for (let i = 0; i < srtJson.length; i++) {
    if (srtJson[i].text === '***') feedback += '\n\n';
    else feedback += srtJson[i].text;
  }
  functions.logger.info('feedback: ', feedback);
  return feedback;
}

// AWS SNS endpoint for subscription verficiation
// exports.awsSns = functions.https.onRequest(async (req, res) => {
//   let payload = JSON.parse(req.body);
//   console.log('payload: ', payload);
//   try {
//     if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
//       const url = payload.SubscribeURL;
//       await request(url, handleSubscriptionResponse);
//     } else if (req.header('x-amz-sns-message-type') === 'Notification') {
//       console.log(payload);
//     } else {
//       throw new Error(`Invalid message type ${payload.Type}`);
//     }
//   } catch (err) {
//     console.error(err);
//     res.status(500).send('Oops');
//   }
//   res.send('Ok');
// });

// const handleSubscriptionResponse = function(error, response) {
//   if (!error && response.statusCode === 200) {
//     console.log('Yes! We have accepted the confirmation from AWS.');
//   } else {
//     throw new Error(`Unable to subscribe to given URL`);
//   }
// };
