const functions = require('firebase-functions');
const admin = require('firebase-admin');
const auth = admin.auth();
const environment = functions.config();
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const db = admin.firestore();
const axios = require('axios');
const cors = require('cors')({ origin: true });
const vttConvert = require('aws-transcription-to-vtt');
const _ = require('lodash');
const webvtt = require('node-webvtt');
const INTRO_LENGTH = 11;
const TRANSITION_LENGTH = 4;

exports.getSubtitles = functions.https.onRequest(async (req, res) => {
  cors(req, res, async () => {
    // return res.status(200).send();

    const data = req.query;
    if (!req.query || !data.submission) {
      return res.status(404).send();
    }
    // exports.getSubtitles = functions.https.onCall(async (data, res) => {
    // console.log(`query: ${data}`);

    try {
      //get media for the given submission:
      let cfurl = (await db.collection('config').doc('meta').get()).data().cloudfronturl;

      //get the submission, and then list the media objects in the order they came in (rather than the order they were transcribed):

      let submission = (await db.collection('submissions').doc(data.submission).get()).data();

      let media_list = submission.media;

      // console.log(media_list);

      //get number of raw videos so we know what they are:
      let media = await db
        .collection('media')
        .where('submission', '==', `submissions/${data.submission}`)
        .where('type', '==', 'raw')
        .get();

      // console.log(`${media.size} videos`);
      // let ids = [];

      let downloads = [];
      let counter = 0;
      media_list.forEach(doc => {
        if (counter < media.size) {
          const url = `${cfurl}/submissions/${data.submission}/${doc.id}/${doc.id}.mp4.json`;
          downloads.push(axios.get(url));
        }
        counter++;
      });

      // console.log(ids);

      // let touse = [];

      // for (let oj of media_list){
      //   if ()
      // }

      // media.forEach(doc => {
      // const url = `${cfurl}/submissions/${data.submission}/${doc.id}/${doc.id}.mp4.json`;
      // downloads.push(axios.get(url));
      // });

      // console.log(downloads);

      let jsons = await Promise.all(downloads);

      // let vtt = ['WEBVTT'];

      let mainCounter = 1;
      //time at which first title ends:
      let startingTime = INTRO_LENGTH;

      let masterCueList = [];

      for (let j of jsons) {
        let vt = vttConvert(j.data);

        let parsed = webvtt.parse(vt);

        // console.log(parsed.cues.length);

        for (let i = 0; i < parsed.cues.length; i++) {
          // console.log(`frozen: ${Object.isFrozen(parsed.cues[i])}`);
          parsed.cues[i].identifier = mainCounter;
          parsed.cues[i].start = parsed.cues[i].start + startingTime;
          parsed.cues[i].end = parsed.cues[i].end + startingTime;

          masterCueList.push(parsed.cues[i]);

          mainCounter++;
        }

        // console.log(parsed.cues);

        //ADD TRANSITION TIME BETWEEN VIDEOS
        startingTime = _.last(parsed.cues).end + TRANSITION_LENGTH;
      }

      return res.send(webvtt.compile({ cues: masterCueList, valid: true }));
    } catch (e) {
      return res.status(500).send();
    }
  });
});
