const functions = require('firebase-functions');
const admin = require('firebase-admin');
const FieldValue = admin.firestore.FieldValue;

/**
 * Create a new media object in the firestore and associate it with a submission.
 * This function returns the path to this media's source in the S3 bucket. (the actual
 * asset doesn't exist yet, but that is the path where the asset should be uploaded)
 *
 * @param {string} type - the type of the media object
 * @param {firebase.firestore.DocumentSnapshot<T>} submissionRef - the submission to associate the new media object with
 * @param {string} exportedAssetName - the name of the asset to the media obj refers to
 * @param {string} language - lanaguage of the submission
 *
 * @returns {object} The path to export the edited video to and the id of the new media object
 */
exports.newMediaObj = async (type, submissionSnapshot, exportedAssetName, language) => {
  const docRef = await admin
    .firestore()
    .collection('media')
    .add({
      type: type,
      srcLang: language,
      status: 'exporting',
      createdAt: FieldValue.serverTimestamp(),
      submission: `submissions/${submissionSnapshot.id}`,
    });
  submissionSnapshot.ref.update({
    media: admin.firestore.FieldValue.arrayUnion(docRef),
  });

  const exportPath = `submissions/${submissionSnapshot.id}/${docRef.id}/${exportedAssetName}`;
  await docRef.update({ src: `s3://ycop27/${exportPath}` });
  return { exportPath, newMediaId: docRef.id, newMediaRef: docRef };
};

/**
 * Get the edit templates for a given phase.
 *
 * The edit templates are stored in the firebase under config/edit_templates.
 * Each phase has a list of edits associated with it.
 *
 * @param {number} phase - the phase to get the edit templates for
 * @returns {object[]} the array of edits for the given phase
 */
// exports.getEdits = async phase => {
//   functions.logger.log(`Getting edits for phase ${phase}`);
//   const edits = await admin.firestore().collection('config').doc('edit_templates').get();
//   return edits.data()[phase];
// };
