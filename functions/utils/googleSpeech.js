const functions = require('firebase-functions');
const fs = require('fs');
const os = require('os');
const path = require('path');
const admin = require('firebase-admin');
const bucket = admin.storage().bucket();
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const speech = require('@google-cloud/speech').v1p1beta1;
const speechClient = new speech.SpeechClient({});
const BUCKET_NAME = 'cop27-1fce1';

/**
 * Google Speech-to-Text call to generate transcriptions of the audio received from
 * AWS Media Convert jobs
 *
 * @param {*} destination Destination key of the audio file
 * @param {*} moderation Whether the job is related to moderation or not
 * @param {*} language Language of the submission
 */
async function speechCall(destination, moderation, language) {
  try {
    const gcsUri = `gs://${bucket.name}/${destination}`;

    // The audio file's encoding, sample rate in hertz, and BCP-47 language code
    const audio = {
      uri: gcsUri,
    };

    const config = {
      encoding: 'MP3',
      sampleRateHertz: 16000,
      languageCode: language,
    };

    // Set the `enableWordTimeOffsets` flat to be true only for non moderation contents
    if (!moderation) config.enableWordTimeOffsets = true;

    const request = {
      audio: audio,
      config: config,
    };

    const [operation] = await speechClient.longRunningRecognize(request);
    const [response] = await operation.promise();
    const transcription = response.results.map(result => result.alternatives[0].transcript).join('\n');

    // If moderation, return the combined transcription
    if (moderation) return transcription;
    // Otherwise, generate VTT file to be upload to YouTube
    else return generateVTT(response, language, (binSize = 5));
  } catch (error) {
    functions.logger.error(error);
  }
}

/**
 * Download S3 audio resource and push the file to Firebase Storage
 *
 * @param {*} submissionId Submission ID
 * @param {*} audioKey The S3 object key of the .mp3 file
 * @param {*} moderation Whether the job is related to moderation or not
 * @param {*} language Language for the submission
 */
async function downloadS3Resource(submissionId, audioKey, moderation, language) {
  try {
    const tempArr = audioKey.split('/');
    const filePath = path.join(os.tmpdir(), tempArr[tempArr.length - 1]);
    functions.logger.info('filePath: ', filePath);

    const options = {
      Bucket: BUCKET_NAME,
      Key: audioKey.replace('s3://cop27/', ''),
    };

    let fileStream = fs.createWriteStream(filePath);
    let s3Stream = S3.getObject(options).createReadStream();

    // Listen for errors returned by the service
    s3Stream.on('error', function (err) {
      // NoSuchKey: The specified key does not exist
      console.error(err);
    });

    s3Stream
      .pipe(fileStream)
      .on('error', function (err) {
        // Capture any errors that occur when writing data to the file
        console.error('File Stream Error:', err);
      })
      .on('close', async function () {
        // Get file details including metadata and post file to Firebase.
        const contentType = 'audio/mpeg';
        const metadata = { contentType: contentType };

        let destination;
        if (moderation)
          destination = `submissions/${submissionId}/moderation_${language}_${tempArr[tempArr.length - 1]}`;
        else destination = `submissions/${submissionId}/${language}_${tempArr[tempArr.length - 1]}`;
        functions.logger.info('Firebase Storage Destination: ', destination);

        await bucket.upload(filePath, {
          destination: destination,
          metadata: metadata,
          resumable: false,
        });
        fs.unlinkSync(filePath);
      });
  } catch (error) {
    functions.logger.error(error);
  }
}

/**
 * Generate a VTT file based on the input response received from Google Speech-to-Text
 * transcription service
 *
 * @param {*} response Response received from Google Speech-to-Text
 * @param {*} binSize Maximum duration for each VTT chunk
 */
function generateVTT(response, language, binSize = 5) {
  let convertedOutput = `WEBVTT\nKind: captions\nLanguage: ${language}\n\n`;
  let startSec = 0;
  let startNano = 0;
  let index = 1;

  for (let idx = 0; idx < response.results.length; idx++) {
    const result = response.results[idx];
    const words = result.alternatives[0].words;
    let transcript = '';

    for (let i = 0; i < words.length; i++) {
      let { startTime, endTime, word } = words[i];

      // Reformat the startTime & endTime to append 0 seconds and nanos
      startTime.seconds = startTime.seconds ? startTime.seconds : 0;
      startTime.nanos = startTime.nanos ? startTime.nanos : 0;
      endTime.seconds = endTime.seconds ? endTime.seconds : 0;
      endTime.nanos = endTime.nanos ? endTime.nanos : 0;

      const timeDiff = endTime.seconds - startSec + (endTime.nanos - startNano) * 0.001 * 0.001 * 0.001;

      // If still within the duration, append the word to the transcript
      if (timeDiff < binSize) transcript = transcript + ' ' + word;
      // Otherwise, reset the startTime and transcript, push the transcript to the array
      else {
        // Convert the start and end time into seconds
        const startInSeconds = startSec * 1 + startNano * 0.001 * 0.001 * 0.001;
        const endInSeconds = startTime.seconds * 1 + startTime.nanos * 0.001 * 0.001 * 0.001;

        // Convert the start and end time into VTT formats
        const formattedStart = secondsToMinutes(startInSeconds);
        const formattedEnd = secondsToMinutes(endInSeconds);

        // Update the convertedOutput VTT
        // convertedOutput += `${index++}\n`;
        convertedOutput += formattedStart + ' --> ' + formattedEnd + '\n';
        convertedOutput += `${transcript}\n\n`;

        // Reset the start timestamps
        startSec = startTime.seconds;
        startNano = startTime.nanos;
        transcript = word;
      }
    }
  }
  functions.logger.info('convertedOutput: ', convertedOutput);
  return convertedOutput;
}

/**
 * Padding the input string with leading zeros
 *
 * @param {*} string Input string
 * @param {*} length Length of the string
 */
function padString(string, length) {
  return (new Array(length + 1).join('0') + string).slice(-length);
}

/**
 * Convert seconds into VTT format start time and end time
 * @param {*} seconds Timestamp in seconds
 */
function secondsToMinutes(seconds) {
  let hours;
  let minutes;
  hours = Math.floor(seconds / 3600);
  seconds = seconds - hours * 3600;
  minutes = Math.floor(seconds / 60);
  seconds = (seconds - minutes * 60).toFixed(3);
  return padString(hours, 2) + ':' + padString(minutes, 2) + ':' + padString(seconds, 6);
}

exports.speechCall = speechCall;
exports.downloadS3Resource = downloadS3Resource;
