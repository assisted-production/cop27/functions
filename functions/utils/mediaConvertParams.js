/**
 * Return the media conver parameters based on the input file url and name modifier
 *
 * @param {String} fileInput S3 or Adobe Spark video url for transcoding
 * @param {String} fileNameModifier Name modifier for the media convert job
 * @param {String} filePath Video file path in S3
 */
function getMediaConvertParams(fileInput, fileNameModifier, filePath) {
  return {
    Settings: {
      AdAvailOffset: 0,
      Inputs: [
        {
          FilterEnable: 'AUTO',
          PsiControl: 'USE_PSI',
          FilterStrength: 0,
          DeblockFilter: 'DISABLED',
          DenoiseFilter: 'DISABLED',
          TimecodeSource: 'EMBEDDED',
          VideoSelector: {
            ColorSpace: 'FOLLOW',
            Rotate: 'AUTO',
            AlphaBehavior: 'DISCARD',
          },
          AudioSelectors: {
            'Audio Selector 1': {
              Offset: 0,
              DefaultSelection: 'DEFAULT',
              ProgramSelection: 1,
              SelectorType: 'TRACK',
              Tracks: [1],
            },
          },
          FileInput: fileInput,
        },
      ],
      OutputGroups: [
        {
          Name: 'File Group',
          OutputGroupSettings: {
            Type: 'FILE_GROUP_SETTINGS',
            FileGroupSettings: {
              Destination: filePath,
            },
          },
          Outputs: [
            {
              VideoDescription: {
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    InterlaceMode: 'PROGRESSIVE',
                    NumberReferenceFrames: 3,
                    Syntax: 'DEFAULT',
                    Softness: 0,
                    GopClosedCadence: 1,
                    GopSize: 90,
                    Slices: 1,
                    GopBReference: 'DISABLED',
                    SlowPal: 'DISABLED',
                    SpatialAdaptiveQuantization: 'ENABLED',
                    TemporalAdaptiveQuantization: 'ENABLED',
                    FlickerAdaptiveQuantization: 'DISABLED',
                    EntropyEncoding: 'CABAC',
                    FramerateControl: 'INITIALIZE_FROM_SOURCE',
                    RateControlMode: 'CBR',
                    CodecProfile: 'MAIN',
                    Telecine: 'NONE',
                    MinIInterval: 0,
                    AdaptiveQuantization: 'HIGH',
                    CodecLevel: 'AUTO',
                    FieldEncoding: 'PAFF',
                    SceneChangeDetect: 'ENABLED',
                    QualityTuningLevel: 'SINGLE_PASS',
                    FramerateConversionAlgorithm: 'DUPLICATE_DROP',
                    UnregisteredSeiTimecode: 'DISABLED',
                    GopSizeUnits: 'FRAMES',
                    ParControl: 'INITIALIZE_FROM_SOURCE',
                    NumberBFramesBetweenReferenceFrames: 2,
                    RepeatPps: 'DISABLED',
                    DynamicSubGop: 'STATIC',
                    Bitrate: 2000000, // 120 Seconds * 2000000 Bps = 29.2 Mb
                  },
                },
                AfdSignaling: 'NONE',
                DropFrameTimecode: 'ENABLED',
                RespondToAfd: 'NONE',
                ColorMetadata: 'INSERT',
              },
              AudioDescriptions: [
                {
                  AudioTypeControl: 'FOLLOW_INPUT',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      AudioDescriptionBroadcasterMix: 'NORMAL',
                      Bitrate: 96000,
                      RateControlMode: 'CBR',
                      CodecProfile: 'LC',
                      CodingMode: 'CODING_MODE_2_0',
                      RawFormat: 'NONE',
                      SampleRate: 48000,
                      Specification: 'MPEG4',
                    },
                  },
                  LanguageCodeControl: 'FOLLOW_INPUT',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {
                  CslgAtom: 'INCLUDE',
                  CttsVersion: 0,
                  FreeSpaceBox: 'EXCLUDE',
                  MoovPlacement: 'PROGRESSIVE_DOWNLOAD',
                },
              },
              Extension: 'mp4',
              NameModifier: fileNameModifier,
            },
            {
              VideoDescription: {
                Width: 1920,
                Height: 1080,
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    InterlaceMode: 'PROGRESSIVE',
                    NumberReferenceFrames: 3,
                    Syntax: 'DEFAULT',
                    Softness: 0,
                    GopClosedCadence: 1,
                    GopSize: 90,
                    Slices: 1,
                    GopBReference: 'DISABLED',
                    SlowPal: 'DISABLED',
                    SpatialAdaptiveQuantization: 'ENABLED',
                    TemporalAdaptiveQuantization: 'ENABLED',
                    FlickerAdaptiveQuantization: 'DISABLED',
                    EntropyEncoding: 'CABAC',
                    FramerateControl: 'INITIALIZE_FROM_SOURCE',
                    RateControlMode: 'CBR',
                    CodecProfile: 'MAIN',
                    Telecine: 'NONE',
                    MinIInterval: 0,
                    AdaptiveQuantization: 'HIGH',
                    CodecLevel: 'AUTO',
                    FieldEncoding: 'PAFF',
                    SceneChangeDetect: 'ENABLED',
                    QualityTuningLevel: 'SINGLE_PASS',
                    FramerateConversionAlgorithm: 'DUPLICATE_DROP',
                    UnregisteredSeiTimecode: 'DISABLED',
                    GopSizeUnits: 'FRAMES',
                    ParControl: 'INITIALIZE_FROM_SOURCE',
                    NumberBFramesBetweenReferenceFrames: 2,
                    RepeatPps: 'DISABLED',
                    DynamicSubGop: 'STATIC',
                    Bitrate: 4000000, // 120 Seconds * 2000000 Bps = 29.2 Mb
                  },
                },
                AfdSignaling: 'NONE',
                DropFrameTimecode: 'ENABLED',
                RespondToAfd: 'NONE',
                ColorMetadata: 'INSERT',
              },
              AudioDescriptions: [
                {
                  AudioTypeControl: 'FOLLOW_INPUT',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      AudioDescriptionBroadcasterMix: 'NORMAL',
                      Bitrate: 96000,
                      RateControlMode: 'CBR',
                      CodecProfile: 'LC',
                      CodingMode: 'CODING_MODE_2_0',
                      RawFormat: 'NONE',
                      SampleRate: 48000,
                      Specification: 'MPEG4',
                    },
                  },
                  LanguageCodeControl: 'FOLLOW_INPUT',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {
                  CslgAtom: 'INCLUDE',
                  CttsVersion: 0,
                  FreeSpaceBox: 'EXCLUDE',
                  MoovPlacement: 'PROGRESSIVE_DOWNLOAD',
                },
              },
              Extension: 'mp4',
              NameModifier: '_editing',
            },
            {
              VideoDescription: {
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'FRAME_CAPTURE',
                  FrameCaptureSettings: {
                    FramerateNumerator: 1,
                    FramerateDenominator: 5,
                    MaxCaptures: 5,
                    Quality: 85,
                  },
                },
                DropFrameTimecode: 'ENABLED',
                ColorMetadata: 'INSERT',
              },
              ContainerSettings: {
                Container: 'RAW',
              },
              Extension: 'jpg',
              NameModifier: fileNameModifier,
            },
            {
              ContainerSettings: {
                Container: 'RAW',
              },
              AudioDescriptions: [
                {
                  AudioSourceName: 'Audio Selector 1',
                  CodecSettings: {
                    Codec: 'MP3',
                    Mp3Settings: {
                      RateControlMode: 'VBR',
                      SampleRate: 48000,
                      VbrQuality: 2,
                    },
                  },
                },
              ],
              NameModifier: '_audio',
            },
          ],
        },
      ],
    },
    Queue: 'arn:aws:mediaconvert:eu-west-2:031127952371:queues/Default',
    Role: 'arn:aws:iam::031127952371:role/MediaConvert_Default_Role',
    StatusUpdateInterval: 'SECONDS_10',
  };
}

/**
 * Return the media conver parameters based on the input file url and name modifier
 * with generating thumbnails
 *
 * @param {String} fileInput S3 or Adobe Spark video url for transcoding.
 * @param {String} fileNameModifier Name modifier for the media convert job.
 * @param {String} filePath Video file path in S3
 */
function getMediaConvertParamsWithoutThumbnail(fileInput, fileNameModifier, filePath) {
  return {
    Settings: {
      AdAvailOffset: 0,
      Inputs: [
        {
          FilterEnable: 'AUTO',
          PsiControl: 'USE_PSI',
          FilterStrength: 0,
          DeblockFilter: 'DISABLED',
          DenoiseFilter: 'DISABLED',
          TimecodeSource: 'EMBEDDED',
          VideoSelector: {
            ColorSpace: 'FOLLOW',
            Rotate: 'DEGREE_0',
            AlphaBehavior: 'DISCARD',
          },
          AudioSelectors: {
            'Audio Selector 1': {
              Offset: 0,
              DefaultSelection: 'DEFAULT',
              ProgramSelection: 1,
              SelectorType: 'TRACK',
              Tracks: [1],
            },
          },
          FileInput: fileInput,
        },
      ],
      OutputGroups: [
        {
          Name: 'File Group',
          OutputGroupSettings: {
            Type: 'FILE_GROUP_SETTINGS',
            FileGroupSettings: {
              Destination: filePath,
            },
          },
          Outputs: [
            {
              VideoDescription: {
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    InterlaceMode: 'PROGRESSIVE',
                    NumberReferenceFrames: 3,
                    Syntax: 'DEFAULT',
                    Softness: 0,
                    GopClosedCadence: 1,
                    GopSize: 90,
                    Slices: 1,
                    GopBReference: 'DISABLED',
                    SlowPal: 'DISABLED',
                    SpatialAdaptiveQuantization: 'ENABLED',
                    TemporalAdaptiveQuantization: 'ENABLED',
                    FlickerAdaptiveQuantization: 'DISABLED',
                    EntropyEncoding: 'CABAC',
                    FramerateControl: 'INITIALIZE_FROM_SOURCE',
                    RateControlMode: 'CBR',
                    CodecProfile: 'MAIN',
                    Telecine: 'NONE',
                    MinIInterval: 0,
                    AdaptiveQuantization: 'HIGH',
                    CodecLevel: 'AUTO',
                    FieldEncoding: 'PAFF',
                    SceneChangeDetect: 'ENABLED',
                    QualityTuningLevel: 'SINGLE_PASS',
                    FramerateConversionAlgorithm: 'DUPLICATE_DROP',
                    UnregisteredSeiTimecode: 'DISABLED',
                    GopSizeUnits: 'FRAMES',
                    ParControl: 'INITIALIZE_FROM_SOURCE',
                    NumberBFramesBetweenReferenceFrames: 2,
                    RepeatPps: 'DISABLED',
                    DynamicSubGop: 'STATIC',
                    Bitrate: 2000000, // 120 Seconds * 2000000 Bps = 29.2 Mb
                  },
                },
                AfdSignaling: 'NONE',
                DropFrameTimecode: 'ENABLED',
                RespondToAfd: 'NONE',
                ColorMetadata: 'INSERT',
              },
              AudioDescriptions: [
                {
                  AudioTypeControl: 'FOLLOW_INPUT',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      AudioDescriptionBroadcasterMix: 'NORMAL',
                      Bitrate: 96000,
                      RateControlMode: 'CBR',
                      CodecProfile: 'LC',
                      CodingMode: 'CODING_MODE_2_0',
                      RawFormat: 'NONE',
                      SampleRate: 48000,
                      Specification: 'MPEG4',
                    },
                  },
                  LanguageCodeControl: 'FOLLOW_INPUT',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {
                  CslgAtom: 'INCLUDE',
                  CttsVersion: 0,
                  FreeSpaceBox: 'EXCLUDE',
                  MoovPlacement: 'PROGRESSIVE_DOWNLOAD',
                },
              },
              Extension: 'mp4',
              NameModifier: fileNameModifier,
            },
            {
              ContainerSettings: {
                Container: 'RAW',
              },
              AudioDescriptions: [
                {
                  AudioSourceName: 'Audio Selector 1',
                  CodecSettings: {
                    Codec: 'MP3',
                    Mp3Settings: {
                      RateControlMode: 'VBR',
                      SampleRate: 48000,
                      VbrQuality: 2,
                    },
                  },
                },
              ],
              NameModifier: '_audio',
            },
          ],
        },
      ],
    },
    Queue: 'arn:aws:mediaconvert:eu-west-2:031127952371:queues/Default',
    Role: 'arn:aws:iam::031127952371:role/MediaConvert_Default_Role',
    StatusUpdateInterval: 'SECONDS_10',
  };
}

/**
 * Return the media conver parameters based on the input file url and name modifier
 * with generating thumbnails
 *
 * @param {String} fileInput S3
 * @param {String} fileNameModifier Name modifier for the media convert job.
 * @param {String} filePath Video file path in S3
 */
function getMediaConvertParamsEditing(edit, fileNameModifier, filePath) {
  let initial = {
    Settings: {
      AdAvailOffset: 0,
      Inputs: [],
      OutputGroups: [
        {
          Name: 'Submission Edit',
          OutputGroupSettings: {
            Type: 'FILE_GROUP_SETTINGS',
            FileGroupSettings: {
              Destination: `${filePath}`,
            },
          },
          Outputs: [
            {
              VideoDescription: {
                Width: 1920,
                Height: 1080,
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    InterlaceMode: 'PROGRESSIVE',
                    NumberReferenceFrames: 3,
                    Syntax: 'DEFAULT',
                    Softness: 0,
                    GopClosedCadence: 1,
                    GopSize: 90,
                    Slices: 1,
                    GopBReference: 'DISABLED',
                    SlowPal: 'DISABLED',
                    SpatialAdaptiveQuantization: 'ENABLED',
                    TemporalAdaptiveQuantization: 'ENABLED',
                    FlickerAdaptiveQuantization: 'DISABLED',
                    EntropyEncoding: 'CABAC',
                    FramerateControl: 'INITIALIZE_FROM_SOURCE',
                    RateControlMode: 'CBR',
                    CodecProfile: 'MAIN',
                    Telecine: 'NONE',
                    MinIInterval: 0,
                    AdaptiveQuantization: 'HIGH',
                    CodecLevel: 'AUTO',
                    FieldEncoding: 'PAFF',
                    SceneChangeDetect: 'ENABLED',
                    QualityTuningLevel: 'SINGLE_PASS',
                    FramerateConversionAlgorithm: 'DUPLICATE_DROP',
                    UnregisteredSeiTimecode: 'DISABLED',
                    GopSizeUnits: 'FRAMES',
                    ParControl: 'INITIALIZE_FROM_SOURCE',
                    NumberBFramesBetweenReferenceFrames: 2,
                    RepeatPps: 'DISABLED',
                    DynamicSubGop: 'STATIC',
                    Bitrate: 4000000, // 120 Seconds * 2000000 Bps = 29.2 Mb
                  },
                },
                AfdSignaling: 'NONE',
                DropFrameTimecode: 'ENABLED',
                RespondToAfd: 'NONE',
                ColorMetadata: 'INSERT',
              },
              AudioDescriptions: [
                {
                  AudioTypeControl: 'FOLLOW_INPUT',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      AudioDescriptionBroadcasterMix: 'NORMAL',
                      Bitrate: 96000,
                      RateControlMode: 'CBR',
                      CodecProfile: 'LC',
                      CodingMode: 'CODING_MODE_2_0',
                      RawFormat: 'NONE',
                      SampleRate: 48000,
                      Specification: 'MPEG4',
                    },
                  },
                  LanguageCodeControl: 'FOLLOW_INPUT',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {
                  CslgAtom: 'INCLUDE',
                  CttsVersion: 0,
                  FreeSpaceBox: 'EXCLUDE',
                  MoovPlacement: 'PROGRESSIVE_DOWNLOAD',
                },
              },
              Extension: 'mp4',
            },
            {
              VideoDescription: {
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    RateControlMode: 'QVBR',
                    SceneChangeDetect: 'TRANSITION_DETECTION',
                    MaxBitrate: 1000000,
                    FramerateControl: 'SPECIFIED',
                    FramerateNumerator: 25,
                    FramerateDenominator: 1,
                  },
                },
                Width: 640,
                Height: 360,
              },
              AudioDescriptions: [
                {
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      Bitrate: 64000,
                      CodingMode: 'CODING_MODE_1_0',
                      SampleRate: 44100,
                    },
                  },
                  AudioSourceName: 'Audio Selector 1',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {},
              },
              NameModifier: '_transcoded',
              Extension: 'mp4',
            },
            {
              VideoDescription: {
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    RateControlMode: 'QVBR',
                    SceneChangeDetect: 'TRANSITION_DETECTION',
                    MaxBitrate: 700000,
                    FramerateControl: 'SPECIFIED',
                    FramerateNumerator: 25,
                    FramerateDenominator: 1,
                  },
                },
                Width: 320,
                Height: 180,
              },
              AudioDescriptions: [
                {
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      Bitrate: 64000,
                      CodingMode: 'CODING_MODE_1_0',
                      SampleRate: 44100,
                    },
                  },
                  AudioSourceName: 'Audio Selector 1',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {},
              },
              NameModifier: '_tiny',
              Extension: 'mp4',
            },
          ],
        },
      ],
    },
    Queue: 'arn:aws:mediaconvert:eu-west-2:031127952371:queues/Default',
    Role: 'arn:aws:iam::031127952371:role/MediaConvert_Default_Role',
    StatusUpdateInterval: 'SECONDS_10',
  };

  for (const file of edit.timeline) {
    let inputObj = {
      FilterEnable: 'AUTO',
      PsiControl: 'USE_PSI',
      FilterStrength: 0,
      DeblockFilter: 'DISABLED',
      DenoiseFilter: 'DISABLED',
      TimecodeSource: 'EMBEDDED',
      VideoSelector: {
        ColorSpace: 'FOLLOW',
        Rotate: 'DEGREE_0',
        AlphaBehavior: 'DISCARD',
      },
      AudioSelectors: {
        'Audio Selector 1': {
          Offset: 0,
          DefaultSelection: 'DEFAULT',
          ProgramSelection: 1,
          SelectorType: 'TRACK',
          Tracks: [1],
        },
      },
      FileInput: file.src,
    };

    if (file.overlay) {
      let overlays = [];

      for (let ov of file.overlay) {
        overlays.push({
          Opacity: 100,
          ImageInserterInput: ov,
          FadeOut: 300,
          FadeIn: 300,
          Width: 1920,
          Height: 1080,
          ImageX: 0,
          ImageY: 0,
          Layer: 0,
        });
      }

      inputObj.ImageInserter = {
        InsertableImages: overlays,
      };
    }

    initial.Settings.Inputs.push(inputObj);
  }

  if (edit.watermark) {
    initial.Settings.MotionImageInserter = {
      Input: edit.watermark,
      InsertionMode: 'MOV',
      Offset: {
        ImageX: 130,
        ImageY: 100,
      },
      Playback: 'REPEAT',
    };
  }

  // console.log(file);
  return initial;
}

exports.getMediaConvertParams = getMediaConvertParams;
exports.getMediaConvertParamsWithoutThumbnail = getMediaConvertParamsWithoutThumbnail;
exports.getMediaConvertParamsEditing = getMediaConvertParamsEditing;
